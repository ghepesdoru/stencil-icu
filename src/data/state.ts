import { FormattersCache } from "./formatters"
import { ILocale } from "./types"

export type LocalesCache = { [localeCode: string]: ILocale };

export interface IntlState {
  /**
   * Formatters cache
   */
  formatters: FormattersCache,

  /**
   * Currently loaded locales cache
   */
  locales: LocalesCache,

  /**
   * Current locale code
   */
  active: string,
}

/**
 * Intl state generation mechanism
 */
export function generateState(defaultLocale: string, ...locales: ILocale[]): IntlState {
  return {
    active: defaultLocale,
    locales: locales.reduce((ret, l) => {
      ret[l.code] = l;

      return ret;
    }, {} as LocalesCache),
    formatters: FormattersCache.getInstance(),
  };
}

export const defaultState = generateState(
  'en-US', { code: 'en-US', messages: {} }
)
