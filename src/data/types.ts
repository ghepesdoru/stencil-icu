import { IntlRelativeTimeFormatOptions } from '@formatjs/intl-relativetimeformat';

export interface ILocale {
  code: string,
  messages: {
    [messageId: string]: string,
  }
  dateFormat?: Partial<Intl.DateTimeFormatOptions>,
  timeFormat?: Partial<Intl.DateTimeFormatOptions>,
  numberFormat?: Partial<Intl.NumberFormatOptions>,
  relativeTimeFormat?: Partial<IntlRelativeTimeFormatOptions>,
}
