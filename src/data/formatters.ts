import memoizeFormatConstructor from 'intl-format-cache';
import { IntlMessageFormat } from 'intl-messageformat';
import IntlRelativeFormat, { IntlRelativeTimeFormatOptions } from '@formatjs/intl-relativetimeformat';

type IntlCacheItem = Intl.DateTimeFormat | Intl.NumberFormat | Intl.PluralRules | IntlMessageFormat | IntlRelativeFormat;
type MomoizedFormatter<T> = (locales?: string | string[], options?: T) => any;

type FormatterTypes = 'number' | 'datetime' | 'message' | 'plural' | 'relative';
type Cache = { [type in FormatterTypes]: Record<string, IntlCacheItem> };
type Memoizers = { [type in FormatterTypes]: MomoizedFormatter<Intl.NumberFormatOptions | Intl.DateTimeFormatOptions | Intl.PluralRulesOptions | IntlRelativeTimeFormatOptions> };

// TODO: Add support for ListFormat

export class FormattersCache {
  private static instance: FormattersCache;

  private static allTypes: FormatterTypes[] = ['number', 'datetime', 'message', 'plural', 'relative'];

  public static getInstance(
    types: FormatterTypes[] = FormattersCache.allTypes
  ) {
    if (!FormattersCache.instance) {
      FormattersCache.instance = new FormattersCache(types);
    }

    return FormattersCache.instance;
  }

  /**
   * Formatters cache per type of formatter
   */
  private cache: Cache;

  /**
   * Prepared momoizers
   */
  private memoizers: Memoizers;

  private initializing: Promise<boolean>;

  private constructor(types: FormatterTypes[]) {
    // Initialize the cache for all supported formatter types
    this.cache = types.reduce((ret, t) => {
      ret[t] = {};

      return ret;
    }, {} as Cache);

    let resolve: (value?: boolean) => void;
    this.initializing = new Promise((r) => {
      resolve = r;
    });
    let polyfilled: Promise<any> = Promise.resolve();

    // Determine type requiring polyfilling
    if (types.indexOf('relative') > -1) {
      if (!('RelativeTimeFormat' in Intl)) {
        polyfilled = Promise.all([
          import('@formatjs/intl-relativetimeformat/dist/polyfill'),
          import('@formatjs/intl-relativetimeformat/dist/polyfill-locales'),
        ]);
      }
    }

    polyfilled.then(() => {
      // Initialize the memoizers
      this.memoizers = types.reduce((ret, t) => {
        switch (t) {
          case 'datetime':
            ret[t] = memoizeFormatConstructor(Intl.DateTimeFormat, this.cache.datetime);
            break;

          case 'number':
            ret[t] = memoizeFormatConstructor(Intl.NumberFormat, this.cache.number);
            break;

          case 'plural':
            ret[t] = memoizeFormatConstructor(Intl.PluralRules, this.cache.plural);
            break;

          case 'relative':
            ret[t] = memoizeFormatConstructor((Intl as any).RelativeTimeFormat, this.cache.relative);
            break;
        }

        return ret;
      }, {} as Memoizers);
    }).then(() => {
      // Ensure throwing errors for usage of uninitialized formatter types
      FormattersCache.allTypes.forEach((f) => {
        if (!this.memoizers[f]) {
          // Formatter not required
          Object.defineProperty(this.memoizers, f, {
            value: () => {
              throw new Error(
                `Formatter of type ${f} is not supported based on FormattersCache initialization types array.` +
                `If you wish to use the given formatter type, please consider it as a valid type at initialization time.`
              );
            }
          });
        }
      });
    }).then(() => {
      // Announce the instance as being ready for usage
      resolve();
    });
  }

  /**
   * Promise allowing to wait for the instance to be initialized
   */
  public get ready() {
    return this.initializing;
  }

  /**
   * Get a cached date time formatter instance preconfigured with the specified options
   */
  public getDateTimeFormat(locales: string | string[], options?: Intl.DateTimeFormatOptions): Intl.DateTimeFormat
  public getDateTimeFormat(options: Intl.DateTimeFormatOptions): Intl.DateTimeFormat
  public getDateTimeFormat(): Intl.DateTimeFormat {
    return this.memoizers.datetime(...arguments);
  }

  /**
   * Get a cached number formatter instance preconfigured with the specified options
   */
  public getNumberFormat(locales: string | string[], options: Intl.NumberFormatOptions): Intl.NumberFormat
  public getNumberFormat(options: Intl.NumberFormatOptions): Intl.NumberFormat
  public getNumberFormat(): Intl.NumberFormat {
    return this.memoizers.number(...arguments);
  }

  /**
   * Get a cached pluralization rules instance preconfigured with the specified options
   */
  public getPluralRules(locales: string | string[], options: Intl.PluralRulesOptions): Intl.PluralRules
  public getPluralRules(options: Intl.PluralRulesOptions): Intl.PluralRules
  public getPluralRules(): Intl.PluralRules {
    return this.memoizers.plural(...arguments);
  }

  /**
   * Get a cached message formatter instance preconfigured with the specified options
   */
  public getMessageFormat(message: string, locales: string | string[]): IntlMessageFormat {
    const key = `${(Array.isArray(locales) ? locales : [locales].join(','))}: ${message}`;

    if (!this.cache.message[key]) {
      const a = new IntlMessageFormat(message, locales, undefined, {
        formatters: this,
      });

      this.cache.message[key] = a;
    }

    return this.cache.message[key] as IntlMessageFormat;
  }

  /**
   * Get a cached relative time instance preconfigured with specified options
   */
  public getRelativeTimeFormat(locales: string | string[], options: IntlRelativeTimeFormatOptions): IntlRelativeFormat
  public getRelativeTimeFormat(options: IntlRelativeTimeFormatOptions): IntlRelativeFormat
  public getRelativeTimeFormat(): IntlRelativeFormat {
    return this.memoizers.relative(...arguments);
  }
}
