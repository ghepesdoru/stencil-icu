import { h } from '@stencil/core';
import { createProviderConsumer } from '@stencil/state-tunnel';

import { IntlState, defaultState } from './state';

export const IntlTunnel = createProviderConsumer<IntlState>(
  defaultState,
  (subscribe, child) => (
    <context-consumer subscribe={subscribe} renderer={child} />
  )
)

export default IntlTunnel;
