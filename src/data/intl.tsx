import { FormattableUnit, IntlRelativeTimeFormatOptions } from '@formatjs/intl-relativetimeformat';

import { IntlState } from './state';
import { ILocale } from './types';

export class Intl {
  public static withState(state: IntlState) {
    if (!Intl.instance) {
      Intl.instance = new Intl();
    }

    return Intl.instance.withState(state);
  }

  /**
   * Singleton instance keeper
   */
  private static instance: Intl;

  private state: IntlState;

  private constructor() {}

  /**
   * Change the active locale
   */
  public change(localeCode: string): boolean {
    if (this.state.locales[localeCode]) {
      this.state.active = localeCode;

      return true;
    }

    return false;
  }

  /**
   * Allows addition of locales to the current state
   */
  public add(locale: ILocale): boolean {
    if (!this.state.locales[locale.code]) {
      this.state.locales[locale.code] = locale;

      return true;
    }

    return false;
  }

  /**
   * Format received date to specified or default datet format
   */
  public formatDate(date: Date, options: Intl.DateTimeFormatOptions = {}) {
    const formatter = this.state.formatters.getDateTimeFormat(this.active, {
      ...this.current.dateFormat || {},
      ...options,
    });

    return formatter.format(date);
  }

  /**
   * Format received date to specified or default time format
   */
  public formatTime(date: Date, options: Intl.DateTimeFormatOptions = {}) {
    const formatter = this.state.formatters.getDateTimeFormat(this.active, {
      ...this.current.timeFormat || {},
      ...options,
    });

    return formatter.format(date);
  }

  /**
   * Format received duration to specified unit
   */
  public formatTimeRelative(value: number, unit: FormattableUnit, options: IntlRelativeTimeFormatOptions = {}) {
    const formatter = this.state.formatters.getRelativeTimeFormat(this.active, {
      ...this.current.relativeTimeFormat || {},
      ...options,
    });

    return formatter.format(value, unit);
  }

  /**
   * Format the referenced message while offering interpolation and pluralization support
   */
  public formatMessage(id: string, fallback?: string, data?: Record<any, any>) {
    const message = this.current.messages[id] || fallback || `Non existing message for id '${id}' in locale '${this.active}'.`;
    const formatter = this.state.formatters.getMessageFormat(message, this.active);

    return formatter.format(data);
  }

  /**
   * Format the referenced HTML message while offering interpolation and pluralization support
   */
  public formatHTMLMessage(id: string, fallback?: string, data?: Record<any, any>) {
    const message = this.current.messages[id] || fallback || `Non existing message for id '${id}' in locale '${this.active}'.`;
    const formatter = this.state.formatters.getMessageFormat(message, this.active);

    return formatter.formatHTMLMessage(data);
  }

  public formatNumber(value: number, options: Intl.NumberFormatOptions = {}) {
    const formatter = this.state.formatters.getNumberFormat(this.active, {
      ...this.current.numberFormat || {},
      ...options,
    });

    return formatter.format(value);
  }

  /**
   * Active locale code getter
   */
  public get active() {
    return this.state.active;
  }

  /**
   * Attaches provided state to the global instance
   */
  private withState(state: IntlState) {
    this.state = state;

    return this;
  }

  /**
   * Current locale getter
   */
  private get current() {
    return this.state.locales[this.active];
  }
}
