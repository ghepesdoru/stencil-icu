import { FormattersCache } from './formatters';

describe('FormattersCache', () => {
  beforeEach(() => {
    delete (FormattersCache as any).instance;
  });

  it('provides only the specified formatter types', async () => {
    const f = FormattersCache.getInstance(['number']);

    await f.ready;

    expect(f.getNumberFormat('en-US', {})).toBeTruthy();
    expect(() => f.getPluralRules('en-US', {})).toThrow();
  });

  it('reuses the same formatter for multipla calls with the same params', async () => {
    const f = FormattersCache.getInstance();
    await f.ready;

    const nf = f.getNumberFormat('en-US', {});
    expect(nf).toBe(f.getNumberFormat('en-US', {}));

    const nfCustom = f.getNumberFormat('en-US', { useGrouping: true });
    expect(nf).not.toBe(nfCustom);

    const nfRo = f.getNumberFormat('ro-RO', {});
    expect(nfRo).not.toBe(nf);
  });
});
