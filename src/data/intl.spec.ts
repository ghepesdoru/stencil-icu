import { Intl } from './intl';
import { IntlState, generateState } from './state';
import { locales } from './state.spec';

describe('Int', () => {
  let state: IntlState;

  beforeAll(() => {
    state = generateState('ro-RO', locales[1]);
  });

  beforeEach(() => {
    const i = Intl.withState(state);
    i.change('ro-RO');
  });

  it('allows locale additions', () => {
    expect(Intl.withState(state).add(locales[0])).toEqual(true);
  });

  it('uses provided state', () => {
    expect(Intl.withState(state).active).toEqual('ro-RO');
  });

  it('can change the active locale', () => {
    const i = Intl.withState(state);

    expect(i.active).toEqual('ro-RO');
    expect(i.change('en-US')).toBeTruthy();
    expect(i.active).toEqual('en-US');
    expect(i.change('random-locale')).toBeFalsy();
    expect(i.active).toEqual('en-US');
    expect(i.change('ro-RO')).toBeTruthy();
    expect(i.active).toEqual('ro-RO');
  });

  it('can format dates', () => {
    const i = Intl.withState(state);
    const date = new Date(1580418526656);

    expect(i.formatDate(date)).toEqual('30.01.2020');
    expect(i.formatDate(date, { month: 'numeric' })).toEqual('30.1.2020');

    expect(i.change('en-US')).toBeTruthy();

    expect(i.formatDate(date)).toEqual('1/30/2020');
    expect(i.formatDate(date, { year: '2-digit' })).toEqual('1/30/20');

    expect(i.change('ro-RO')).toBeTruthy();
  });

  it('can format time', () => {
    const i = Intl.withState(state);
    const date = new Date(1580418526656);

    expect(i.formatTime(date)).toEqual('23:08:46');
    expect(i.formatTime(date, { hour12: true })).toEqual('11:08:46 p.m.');

    i.change('en-US');
    expect(i.formatTime(date)).toEqual('11:08:46 PM');
    expect(i.formatTime(date, { hour12: false })).toEqual('23:08:46');
  });


  it('can format relative time', () => {
    const i = Intl.withState(state);

    expect(i.formatTimeRelative(-1, 'day')).toEqual('acum 1 zi');
    expect(i.formatTimeRelative(1, 'day')).toEqual('peste 1 zi');
    expect(i.formatTimeRelative(0, 'second')).toEqual('peste 0 secunde');
    expect(i.formatTimeRelative(10, 'second')).toEqual('peste 10 secunde');

    i.change('en-US');
    expect(i.formatTimeRelative(-1, 'day')).toEqual('1 day ago');
    expect(i.formatTimeRelative(1, 'day')).toEqual('in 1 day');
    expect(i.formatTimeRelative(0, 'second')).toEqual('in 0 seconds');
    expect(i.formatTimeRelative(10, 'second')).toEqual('in 10 seconds');
  });

  it('can format message', () => {
    const i = Intl.withState(state);
    const segments = { first: 'Smith', middle: 'A.', last: 'John' };
    const messageId = 'greeting';

    expect(i.formatMessage(messageId, '', segments))
      .toEqual('Salut! Eu sunt Smith A. John.');
    expect(i.formatMessage(messageId, '', { ...segments, first: undefined }))
      .toEqual('Salut! Eu sunt  A. John.');
    expect(i.formatMessage('non_existing_message', 'Fallback message'))
      .toEqual('Fallback message');

    i.change('en-US');
    expect(i.formatMessage(messageId, '', segments))
      .toEqual('Hello! I\'m Smith A. John.');
    expect(i.formatMessage(messageId, '', { ...segments, first: undefined }))
    .toEqual('Hello! I\'m  A. John.');
    expect(i.formatMessage('non_existing_message', 'Fallback message'))
      .toEqual('Fallback message');
  });

  it('can format HTML messages', () => {
    const i = Intl.withState(state);
    const segments = { first: 'Smith', middle: 'A.', last: 'John' };
    const messageId = 'greetingHtml';

    expect(i.formatMessage(messageId, '', segments))
      .toEqual('<div>Salut! Eu sunt Smith A. <b>John</b></div>');
    expect(i.formatMessage(messageId, '', { ...segments, first: undefined }))
      .toEqual('<div>Salut! Eu sunt  A. <b>John</b></div>');
    expect(i.formatMessage('non_existing_message', '<b>Fallback message</b>'))
      .toEqual('<b>Fallback message</b>');

    i.change('en-US');
    expect(i.formatMessage(messageId, '', segments))
      .toEqual('<div>Hello! I\'m Smith A. <b>John</b></div>');
    expect(i.formatMessage(messageId, '', { ...segments, first: undefined }))
    .toEqual('<div>Hello! I\'m  A. <b>John</b></div>');
    expect(i.formatMessage('non_existing_message', '<b>Fallback message</b>'))
      .toEqual('<b>Fallback message</b>');
  });

  it('can format numbers', () => {
    const i = Intl.withState(state);

    expect(i.formatNumber(123456.789)).toEqual('123.456,789');
    expect(i.formatNumber(123456.789, { maximumFractionDigits: 1 })).toEqual('123.456,8');

    i.change('en-US');
    expect(i.formatNumber(123456.789)).toEqual('123,456.789');
    expect(i.formatNumber(123456.789, { maximumFractionDigits: 1 })).toEqual('123,456.8');
    expect(i.formatNumber(123456.789, { useGrouping: false })).toEqual('123456.789');
  });
});
