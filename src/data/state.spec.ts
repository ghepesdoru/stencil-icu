import { defaultState, generateState } from './state';
import { FormattersCache } from './formatters';
import { ILocale } from './types';

export const locales: ILocale[] = [
  {
    code: 'en-US',
    messages: {
      greeting: 'Hello! I\'m {first} {middle} {last}.',
      greetingHtml: `<div>Hello! I\'m {first} {middle} <b>{last}</b></div>`
    },
    dateFormat: {
      day: 'numeric',
      month: 'numeric',
      timeZone: 'America/New_York',
      year: 'numeric'
    },
    timeFormat: {
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    }
  },
  {
    code: 'ro-RO',
    messages: {
      greeting: 'Salut! Eu sunt {first} {middle} {last}.',
      greetingHtml: `<div>Salut! Eu sunt {first} {middle} <b>{last}</b></div>`
    },
    dateFormat: {
      day: '2-digit',
      month: '2-digit',
      timeZone: 'Europe/Bucharest',
      year: 'numeric'
    },
    timeFormat: {
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    }
  }
];

describe(('IntlState'), () => {
  it('exposes defaultState', () => {
    expect(defaultState).toBeTruthy();
    expect(defaultState.active).toBe('en-US');
    expect(defaultState.formatters).toBeInstanceOf(FormattersCache);
    expect(Object.keys(defaultState.locales).length).toEqual(1);
    expect(defaultState.locales['en-US']).toBeTruthy();
  });

  it('can generate new states', () => {
    const s = generateState('en-US', { code: 'en-US', messages: {} });

    expect(s).toBeTruthy();
    expect(s).toEqual(defaultState);
  });

  it('can generate multiple locales states', () => {
    const s = generateState(locales[1].code, ...locales);

    expect(s).toBeTruthy();
    expect(s.active).toEqual('ro-RO');
    expect(Object.keys(s.locales).length).toEqual(2);
    expect(s.locales['ro-RO']).toEqual(locales[1]);
  });
});
