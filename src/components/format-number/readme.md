# format-number



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                  | Default     |
| --------- | --------- | ----------- | --------------------- | ----------- |
| `options` | --        |             | `NumberFormatOptions` | `undefined` |
| `value`   | `value`   |             | `number`              | `undefined` |


## Dependencies

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  format-number --> context-consumer
  style format-number fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
