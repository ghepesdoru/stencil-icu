import { Component, Prop, h } from '@stencil/core';

import IntlTunnel from '../../data/tunnel';
import { Intl } from '../../data/intl';
import { IntlState } from '../../data/state';

@Component({
  tag: 'format-number',
})
export class FormatNumber {
  @Prop() value: number;

  @Prop() options: Intl.NumberFormatOptions;

  render() {
    return <IntlTunnel.Consumer>
      {
        (state: IntlState) => {
          return Intl.withState(state).formatNumber(this.value, this.options);
        }
      }
    </IntlTunnel.Consumer>;
  }
}
