import { Component, Prop, h } from '@stencil/core';

import IntlTunnel from '../../data/tunnel';
import { Intl } from '../../data/intl';
import { IntlState } from '../../data/state';

@Component({
  tag: 'format-message',
})
export class FormatMessage {
  @Prop() message: string;

  @Prop() values: Record<any, any>;

  @Prop() default: string;

  render() {
    return <IntlTunnel.Consumer>
      {
        (state: IntlState) => {
          return Intl.withState(state).formatMessage(this.message, this.default, this.values);
        }
      }
    </IntlTunnel.Consumer>;
  }
}
