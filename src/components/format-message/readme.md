# format-message



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                    | Default     |
| --------- | --------- | ----------- | ----------------------- | ----------- |
| `default` | `default` |             | `string`                | `undefined` |
| `message` | `message` |             | `string`                | `undefined` |
| `values`  | --        |             | `{ [x: string]: any; }` | `undefined` |


## Dependencies

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  format-message --> context-consumer
  style format-message fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
