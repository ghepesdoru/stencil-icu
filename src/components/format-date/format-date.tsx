import { Component, Prop, h } from '@stencil/core';

import IntlTunnel from '../../data/tunnel';
import { Intl } from '../../data/intl';
import { IntlState } from '../../data/state';

@Component({
  tag: 'format-date',
})
export class FormatDate {
  @Prop() value: Date | number | string;

  @Prop() options: Intl.DateTimeFormatOptions;

  render() {
    return <IntlTunnel.Consumer>
      {
        (state: IntlState) => {
          return Intl.withState(state).formatDate(new Date(this.value), this.options || {});
        }
      }
    </IntlTunnel.Consumer>;
  }
}
