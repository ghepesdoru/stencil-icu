import { Component, Prop, h, State } from '@stencil/core';

import IntlTunnel from '../../data/tunnel';
import { IntlState, generateState } from '../../data/state';
import { ILocale } from '../../data/types';

@Component({
  tag: 'intl-provider',
})
export class IntlProvider {
  @Prop() locales: ILocale[];

  @Prop() defaultLocale?: ILocale;

  @State() ready: boolean = false;

  private intlState: IntlState;

  async componentWillLoad() {
    this.intlState = generateState(this.defaultCode, ...this.allLocales);

    // Await for the formatters to be polyfilled if required
    await this.intlState.formatters.ready;
    this.ready = true;
  }

  render() {
    return <IntlTunnel.Provider state={this.intlState}>
      {
        this.ready ?
          <slot /> :
          <slot name='loader' />
      }
    </IntlTunnel.Provider>;
  }

  private get defaultCode() {
    return (this.defaultLocale || this.locales[0] || {}).code || 'en-US';
  }

  private get allLocales() {
    const locales = this.locales;

    if (this.defaultLocale) {
      const defaultCode = this.defaultCode;

      if (!this.locales.some((l) => l.code === defaultCode)) {
        // Append the default locale to the locales array in case it is missing
        locales.push(this.defaultLocale);
      }
    }

    return locales;
  }
}
