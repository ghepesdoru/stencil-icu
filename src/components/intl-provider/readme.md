# intl-provider



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute | Description | Type        | Default     |
| --------------- | --------- | ----------- | ----------- | ----------- |
| `defaultLocale` | --        |             | `ILocale`   | `undefined` |
| `locales`       | --        |             | `ILocale[]` | `undefined` |


## Dependencies

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  intl-provider --> context-consumer
  style intl-provider fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
