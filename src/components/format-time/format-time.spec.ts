import { newSpecPage } from '@stencil/core/testing';
import { FormatTime } from './format-time';

describe('FormatTime', () => {
  it('renders', async () => {
    const value = new Date();

    const page = await newSpecPage({
      components: [FormatTime],
      html: `<format-time value=${value}></format-time>`,
    });
  });
});
