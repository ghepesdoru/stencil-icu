# format-time



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                       | Default     |
| --------- | --------- | ----------- | -------------------------- | ----------- |
| `options` | --        |             | `DateTimeFormatOptions`    | `undefined` |
| `value`   | `value`   |             | `Date \| number \| string` | `undefined` |


## Dependencies

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  format-time --> context-consumer
  style format-time fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
