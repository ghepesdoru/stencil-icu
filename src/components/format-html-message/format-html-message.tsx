import { Component, Prop, h, Element } from '@stencil/core';

import IntlTunnel from '../../data/tunnel';
import { Intl } from '../../data/intl';
import { IntlState } from '../../data/state';

@Component({
  tag: 'format-html-message',
})
export class FormatHTMLMessage {
  @Prop() message: string;

  @Prop() values: Record<any, any>;

  @Prop() default: string;

  @Element() el: HTMLElement;

  render() {
    return <IntlTunnel.Consumer>
      {
        (state: IntlState) => {
          this.el.innerHTML = Intl.withState(state).formatHTMLMessage(this.message, this.default, this.values) as any;
          return null;
        }
      }
    </IntlTunnel.Consumer>;
  }
}
