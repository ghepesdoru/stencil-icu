import { Component, Prop, h } from '@stencil/core';
import { IntlRelativeTimeFormatOptions, FormattableUnit } from '@formatjs/intl-relativetimeformat';
import {
  differenceInSeconds, differenceInMinutes, differenceInWeeks, differenceInMonths, differenceInQuarters, differenceInYears, differenceInHours, differenceInDays,
} from 'date-fns';

import IntlTunnel from '../../data/tunnel';
import { Intl } from '../../data/intl';
import { IntlState } from '../../data/state';

@Component({
  tag: 'format-relative',
})
export class FormatRelativeTime {
  private static UnitsReversed: FormattableUnit[] = [
    'year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second'
  ];

  private static computeValue(from: Date, to: Date, unit: FormattableUnit): number {
    switch (unit) {
      case 'second':
      case 'seconds':
        return differenceInSeconds(from, to);

      case 'minute':
      case 'minutes':
        return differenceInMinutes(from, to);

      case 'hour':
      case 'hours':
        return differenceInHours(from, to);

      case 'day':
      case 'days':
        return differenceInDays(from, to);

      case 'week':
      case 'weeks':
        return differenceInWeeks(from, to);

      case 'month':
      case 'months':
        return differenceInMonths(from, to);

      case 'quarter':
      case 'quarters':
        return differenceInQuarters(from, to);

      case 'year':
      case 'years':
        return differenceInYears(from ,to);
    }
  }

  private static determineBestUnitMatch(unit: typeof FormatRelativeTime.prototype.unit, from: Date, to: Date) {
    if ('auto' === unit) {
      for (const u of FormatRelativeTime.UnitsReversed) {
        const v = this.computeValue(from, to, u);

        if (v !== 0) {
          return u;
        }
      }

      return 'seconds'; // Default to seconds
    }

    return unit;
  }

  @Prop() from: Date | number | string;

  @Prop() to: Date | number | string;

  @Prop() unit: FormattableUnit | 'auto';

  @Prop() options: IntlRelativeTimeFormatOptions;

  render() {
    const from = new Date(this.from !== undefined ? this.from: Date.now());
    const to = new Date(this.to !== undefined ? this.from: Date.now());

    const unit = FormatRelativeTime.determineBestUnitMatch(this.unit, from, to);
    const v = FormatRelativeTime.computeValue(from, to, unit);

    return <IntlTunnel.Consumer>
      {
        (state: IntlState) => {
          return Intl.withState(state).formatTimeRelative(v, unit, this.options);
        }
      }
    </IntlTunnel.Consumer>;
  }
}
