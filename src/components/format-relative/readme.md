# format-relative



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                                                                                                                                                                                           | Default     |
| --------- | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `from`    | `from`    |             | `Date \| number \| string`                                                                                                                                                                     | `undefined` |
| `options` | --        |             | `IntlRelativeTimeFormatOptions`                                                                                                                                                                | `undefined` |
| `to`      | `to`      |             | `Date \| number \| string`                                                                                                                                                                     | `undefined` |
| `unit`    | `unit`    |             | `"auto" \| "day" \| "days" \| "hour" \| "hours" \| "minute" \| "minutes" \| "month" \| "months" \| "quarter" \| "quarters" \| "second" \| "seconds" \| "week" \| "weeks" \| "year" \| "years"` | `undefined` |


## Dependencies

### Depends on

- context-consumer

### Graph
```mermaid
graph TD;
  format-relative --> context-consumer
  style format-relative fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
